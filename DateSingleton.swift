//
//  DateSingleton.swift
//  Left4DeadApp
//
//  Created by Di Monda Davide on 15/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import Foundation

class DateSingleton{
    
    static let shared = DateSingleton()
    private var date : String
    private var dateSecondFormat : String
    
    private init(){
        
        date = ""
        dateSecondFormat = ""
        
    }
    
    public func getDateSecondFormat() -> String{
        return dateSecondFormat
    }
    
    public func getDate() -> String{
        return date
    }
    
    public func setDate(d : String){
        date = d
    }
    
    public func setDateSecondFormat(d: String){
        dateSecondFormat = d
    }
    
}
