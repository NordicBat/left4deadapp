//
//  ViewController.swift
//  Left4DeadApp
//
//  Created by Gabriel de Freitas Meira on 06/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import UIKit
import CoreData
import HealthKit
import WatchKit
import WatchConnectivity
import AVFoundation

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: Outlet
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var circle: UIImageView!
    @IBOutlet weak var startAnimationButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var selectExerciseCollectionView: UICollectionView!
    @IBOutlet weak var circleExt: UIImageView!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var check: UIImageView!
    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var indicatorLabel: UILabel!
    
    // MARK: Variable declaration
    let exerciseArray = ["Relaxing Breath", "Stimulating Breath", "Deep Breath"]
    var flag : String = "null"
    
    var stopAn : Bool = false
    
    let healthStore = HKHealthStore()
    var hkAuth = UserDefaults.standard.bool(forKey: "HKAuth")
    
    //    let mindfulType = HKObjectType.categoryType(forIdentifier: .mindfulSession)
    let heartRateType = HKQuantityType.quantityType(forIdentifier: .heartRate)!
    let heartRateUnit = HKUnit.init(from: "count/min")

    var initBpm : Int = 0
    var finBpm : Int = 0

    
    // MARK: didLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if !hkAuth {
            self.showAuthAlert()
        }
        labelIndicator(update: false)
        // Setting images
        self.circleExt.alpha = 0
        self.stopButton.alpha = 0
        
        check.isHidden = true
        stopButton.isHidden = true
        
        // Setting pages
        pageControl.hidesForSinglePage = true
        selectExerciseCollectionView.backgroundColor = UIColor.clear
        
        // Setting vector images
        background.adjustsImageSizeForAccessibilityContentSizeCategory = true
        circle.adjustsImageSizeForAccessibilityContentSizeCategory = true
        
        startAnimationButton.isHidden = true
        
       
        
        self.bpmLabel.text = String(initBpm)
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        // Set white status bar
        return .lightContent
    }
    
    // MARK: Setting page control
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        print(pageControl.currentPage)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        print(pageControl.currentPage)
    }
    
    
    // MARK: Setting collection view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        pageControl.numberOfPages = exerciseArray.count
        
        return exerciseArray.count
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectExerciseCell", for: indexPath) as! SelectExerciseCell
        
        cell.exerciseLabel.text = exerciseArray[indexPath.section]
        
        
        return cell
        
    }
    
    
    // MARK: Select exercise
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = selectExerciseCollectionView.cellForItem(at: indexPath) as? SelectExerciseCell{
            
            // Test
            print("Cell selected")
            flag  = cell.exerciseLabel.text!
            print(flag)
            
            
            // MARK: select exercise animations
            UIView.animate(withDuration: 0.5, animations: {
                self.selectExerciseCollectionView.alpha = 0
                self.pageControl.alpha = 0
                self.startAnimationButton.alpha = 0
            }) { (finished : Bool) in
                self.selectExerciseCollectionView.isHidden = true
                self.pageControl.isHidden = true
                
                self.startAnimationButton.isHidden = false
                UIView.animate(withDuration: 1, animations: {
                    self.startAnimationButton.alpha = 1
                }) { (finished : Bool) in
                    
                }
                
            }
            
        }
        
    }
    
    
    // MARK: Breathing animation
    @IBAction func startAnimation(_ sender: Any) {

        
        // Hiding START and showing INTERRUPT
        self.stopButton.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.startAnimationButton.alpha = 0
            self.stopButton.alpha = 1
        }) { (finished : Bool) in
            self.startAnimationButton.isHidden = true
        }
        
        stopAn = true
        
        if (flag == "Relaxing Breath"){
            
            self.firstBreathingAnimation()
            
        }else if(flag == "Stimulating Breath"){
            
            self.secondBreathingAnimation()
            
        }else if(flag == "Deep Breath"){
            
            self.thirdBreathingAnimation()
            
        }
    }
    
    
    // MARK: first animation
    private func firstBreathingAnimation(){
        
        UIView.animateKeyframes(withDuration: 19, delay: 0.0, options: [.repeat, .calculationModeCubic], animations: {
            
            UIView.setAnimationRepeatCount(3)

            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.21, animations: {
                let widening = CGAffineTransform(scaleX: 1.35, y: 1.35)
                self.circle.transform = widening
            })

            
            // Hold
            UIView.addKeyframe(withRelativeStartTime: 0.21, relativeDuration: 0.18, animations: {
                self.circleExt.alpha = 1
            })

            UIView.addKeyframe(withRelativeStartTime: 0.39, relativeDuration: 0.18, animations: {
//                self.indicatorLabel.text = "Exhale"
                self.circleExt.alpha = 0
                
                
            })
            UIView.addKeyframe(withRelativeStartTime: 0.57, relativeDuration: 0.43, animations: {
            
                
                let narrowing = CGAffineTransform(scaleX: 1, y: 1)
                self.circle.transform = narrowing
                
                
            })
            self.labelIndicator(update: true)
            
        }) { (finished : Bool) in
            
            // Check animation
            self.check.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                
                self.check.alpha = 1
                
            }, completion: { (finished : Bool) in
                
                UIView.animate(withDuration: 0.75, animations: {
                    self.check.alpha = 0
                    
                }, completion: { (finished : Bool) in
                    self.check.isHidden = true
                    
                    //Showing EXERCISES
                    self.selectExerciseCollectionView.isHidden = false
                    self.pageControl.isHidden = false
                    self.stopButton.isHidden = true
                    UIView.animate(withDuration: 0.5, animations: {
                        
                        self.selectExerciseCollectionView.alpha = 1
                        self.pageControl.alpha = 1
                       
                        
                    })
                    self.startToMeasure()
                    self.bpmLabel.text = String(self.finBpm)
                    
                    self.saveInCoreDate()
                })
                
            })
            self.labelIndicator(update: false)
        }
    }
    
    
    // MARK: Second animation
    private func secondBreathingAnimation(){
        
        UIView.animateKeyframes(withDuration: 6.5, delay: 0.0, options: [.repeat, .calculationModeCubic,], animations: {
            
            UIView.setAnimationRepeatCount(3)
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.46, animations: {
                
                let widening = CGAffineTransform(scaleX: 1.35, y: 1.35)
                self.circle.transform = widening
                
            })
            
            // Hold
            UIView.addKeyframe(withRelativeStartTime: 0.46, relativeDuration: 0.04, animations: {
                
                self.circleExt.alpha = 0.5
                
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.50, relativeDuration: 0.04, animations: {
                
                self.circleExt.alpha = 0
                
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.54, relativeDuration: 0.46, animations: {
                
                let narrowing = CGAffineTransform(scaleX: 1, y: 1)
                self.circle.transform = narrowing
                
            })
            self.labelIndicator(update: true)

        }) { (finished : Bool) in
            
            // Check animation
            self.check.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                
                self.check.alpha = 1
                
            }, completion: { (finished : Bool) in
                
                UIView.animate(withDuration: 0.75, animations: {
                    
                    self.check.alpha = 0
                    
                }, completion: { (finished : Bool) in
                    
                    self.check.isHidden = true
                    
                    //Showing EXERCISES
                    self.selectExerciseCollectionView.isHidden = false
                    self.pageControl.isHidden = false
                    self.stopButton.isHidden = true
                    UIView.animate(withDuration: 0.5, animations: {
                        
                        self.selectExerciseCollectionView.alpha = 1
                        self.pageControl.alpha = 1
                        
                    })
                    self.startToMeasure()
                    self.bpmLabel.text = String(self.finBpm)
                    
                    self.saveInCoreDate()
                })
            })
            self.labelIndicator(update: false)
        }
    }
    
    
    // MARK: Third animation
    private func thirdBreathingAnimation(){
        
        UIView.animateKeyframes(withDuration: 11, delay: 0.0, options: [.repeat, .calculationModeCubic], animations: {
            
            UIView.setAnimationRepeatCount(3)
            
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.27, animations: {
                
                let widening = CGAffineTransform(scaleX: 1.35, y: 1.35)
                self.circle.transform = widening
                
            })
            
            // Hold
            UIView.addKeyframe(withRelativeStartTime: 0.27, relativeDuration: 0.23, animations: {
                
                self.circleExt.alpha = 0.5
                
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.50, relativeDuration: 0.23, animations: {
                
                self.circleExt.alpha = 0
                
            })
            
            UIView.addKeyframe(withRelativeStartTime: 0.73, relativeDuration: 0.27, animations: {
                
                let narrowing = CGAffineTransform(scaleX: 1, y: 1)
                self.circle.transform = narrowing
                
            })
            self.labelIndicator(update: true)

        }) { (finished : Bool) in
            
            // Check animation
            self.check.isHidden = false
            UIView.animate(withDuration: 0.5, animations: {
                
                self.check.alpha = 1
                
            }, completion: { (finished : Bool) in
                
                UIView.animate(withDuration: 0.75, animations: {
                    
                    self.check.alpha = 0
                    
                }, completion: { (finished : Bool) in
                    
                    self.check.isHidden = true
                    
                    //Showing EXERCISES
                    self.selectExerciseCollectionView.isHidden = false
                    self.pageControl.isHidden = false
                    self.stopButton.isHidden = true
                    UIView.animate(withDuration: 0.5, animations: {
                        
                        self.selectExerciseCollectionView.alpha = 1
                        self.pageControl.alpha = 1
                        
                    })
                    self.startToMeasure()
                    self.bpmLabel.text = String(self.finBpm)
                    
                    self.saveInCoreDate()
                })
            })
            self.labelIndicator(update: false)
        }
        
        
    }
    
    // MARK: Stop the current animation
    @IBAction func stopAnimating(_ sender: Any) {
        
        self.check.isHidden = true
        
        self.view.subviews.forEach({$0.layer.removeAllAnimations()})
        self.view.layer.removeAllAnimations()
        self.view.layoutIfNeeded()
        
        stopButton.isHidden = true
        self.startToMeasure()
        self.bpmLabel.text = String(finBpm)
    }
    
    func saveInCoreDate(){
        
        // MARK: Setting CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        // Setting new entry for Exercise table
        let newExercise = NSEntityDescription.insertNewObject(forEntityName: "Exercise", into: context)
        
        newExercise.setValue(self.flag, forKey: "type")
        newExercise.setValue("\(day) \(month + 1) \(year)", forKey: "date")
        newExercise.setValue("\(self.initBpm) to \(self.finBpm)", forKey: "heartbeat")
        print("\(self.initBpm) to \(self.finBpm)")
        
        // Insert in DB
        do{
            
            try context.save()
            print("SAVED HEARTBEAT")
            
        }catch{
            print("ERROR HEARTBEAT")
        }
        
    }
    
    
    func activateHealthKit() {
        // Define what HealthKit data we want to ask to read
        let typestoRead = Set([
            HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.mindfulSession)!,
            HKSampleType.quantityType(forIdentifier: .heartRate)!
            ])
        
        // Define what HealthKit data we want to ask to write
        let typestoShare = Set([
            HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.mindfulSession)!,
            HKSampleType.quantityType(forIdentifier: .heartRate)!
            ])
        
        // Prompt the User for HealthKit Authorization
        self.healthStore.requestAuthorization(toShare: typestoShare, read: typestoRead) { (success, error) -> Void in
            if !success {
                print("HealthKit Auth error\(String(describing: error))")
            }
            // We'll get to this
            print("Auth granted")
            UserDefaults.standard.set(true, forKey: "HKAuth")
            self.startToMeasure()
        }
    }
    
    func showAuthAlert() {
        let alert = UIAlertController(title: "HealthKit Access", message: """
This application to access the heartbeat from the Apple Watch requires access to your Apple Health data.
Don't worry you can activate it later whenever you want.
""",         preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            return
        }))
        alert.addAction(UIAlertAction(title: "Authorize",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        UserDefaults.standard.set(true, forKey: "HKAuth")
                                        self.activateHealthKit()
                                        
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    //    MARK: It prints at least
    func startToMeasure() {

        self.healthStore.execute(self.createStreamingQuery())
        print("Start Mesure called")
     
       
        
    }


    private func createStreamingQuery() -> HKQuery {
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(-1.0 * 60.0 * 1)
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])

        let query = HKAnchoredObjectQuery(type: heartRateType, predicate: predicate, anchor: nil, limit: Int(HKObjectQueryNoLimit)) {
            (query, samples, deletedObjects, anchor, error) -> Void in
            self.formatSamples(samples: samples)
         }

        query.updateHandler = { (query, samples, deletedObjects, anchor, error) -> Void in
            self.formatSamples(samples: samples)
          }

        print("Query: \(query)")
        return query
    }

    
    private func formatSamples(samples: [HKSample]?) {
        guard let samples = samples as? [HKQuantitySample] else { return }
        guard let quantity = samples.last?.quantity else { return }

        let value = Int(quantity.doubleValue(for: self.heartRateUnit))
        print("HeartRate: \(value)")
        initBpm = value
        finBpm = value
    }
    
    
    func labelIndicator(update: Bool){
        if update {
            self.indicatorLabel.text = "Inhale, hold and exhale"
        } else {
            self.indicatorLabel.text = "Select an exercise and tap to start"
        }
    }
}


