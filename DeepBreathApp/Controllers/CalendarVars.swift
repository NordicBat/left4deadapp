//
//  CalendarVars.swift
//  Left4DeadApp
//
//  Created by Di Monda Davide on 11/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import Foundation

let date = Date()
let calendar = Calendar.current

let day = calendar.component(.day, from: date)
var weekday = calendar.component(.weekday, from: date) - 1
var month = calendar.component(.month, from: date) - 1
var year = calendar.component(.year, from: date)
