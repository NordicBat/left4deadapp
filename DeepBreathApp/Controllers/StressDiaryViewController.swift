//
//  StressDiaryViewController.swift
//  Left4DeadApp
//
//  Created by Di Monda Davide on 12/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import AVFoundation

class StressDiaryViewController : UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var exerciseTableView: UITableView!
    @IBOutlet weak var titleEx: UILabel!
    @IBOutlet weak var recordingTitle: UILabel!
    @IBOutlet weak var playRecorgings: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    var recordedAudioURL: URL!
    var audioFile:AVAudioFile!
    var audioEngine:AVAudioEngine!
    var audioPlayerNode: AVAudioPlayerNode!
    var stopTimer: Timer!
    
    var exereciseArray : [ExerciseRec] = []
    var heartExercise : [String] = []
    var typeExercise : [String] = []
    var tempDate = String()
    var tempType = String()
    var tempHeart = String()
    
    let record = RecordSound()
    
    override func viewDidLoad() {
        
        
        let date = DateSingleton.shared.getDate()
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComp = url.appendingPathComponent("Audio for \(date).wav"){
            let filePath = pathComp.path
            print(filePath)
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                self.playRecorgings.isHidden = false
                self.recordingTitle.isHidden = false
                self.stopButton.isHidden = true
                self.recordingTitle.text = "Note of: \(DateSingleton.shared.getDate())"
                self.recordButton.isHidden = true
            } else {
                self.playRecorgings.isHidden = true
                self.recordingTitle.isHidden = true
                self.stopButton.isHidden = true
                self.recordButton.isHidden = false
                print("Didnt find the file")
            }

        } else {
            print("File path not Found")
        }
        
        //Setting title via Singleton
        navigationItem.title = DateSingleton.shared.getDate()
        
        // MARK: Extracting dates from CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Exercise")
        
        request.returnsObjectsAsFaults = false
        
        do{
            
            let results = try context.fetch(request)
            
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    if let typeRec = result.value(forKey: "type") as? String{
                        tempType = typeRec
                    }
                    if let dateRec = result.value(forKey: "date") as? String{
                        tempDate = dateRec
                    }
                    if let heartRec = result.value(forKey: "heartbeat") as? String{
                        tempHeart = heartRec
                    }
                    let exerciseRec = ExerciseRec.init(d: tempDate, t: tempType, h: tempHeart)
                    exereciseArray.append(exerciseRec)
                }
            }
            
        }catch{
            print("ERROR")
        }
        
        // Filtering results
        for index in 0...exereciseArray.count-1{
            
            if(exereciseArray[index].date == DateSingleton.shared.getDateSecondFormat()){
                typeExercise.append(exereciseArray[index].type)
                heartExercise.append(exereciseArray[index].heart)
            }
        }

        // NavBar trasparent background
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        // TableView trasparent background
        exerciseTableView.backgroundColor = UIColor.clear
                
    }
    
    
    
    @IBAction func recordAudio(_ sender: Any) {
        record.recordAudio(dateName: DateSingleton.shared.getDate())
        
        self.recordButton.isHidden = true
        self.stopButton.isHidden = false
    }
    
    @IBAction func stopRecording(_ sender: Any) {
        record.stopRecording()
        self.stopButton.isHidden = true
        self.playRecorgings.isHidden = false
        self.recordingTitle.isHidden = false
        self.recordingTitle.text = "Note of: \(DateSingleton.shared.getDate())"
    }
 

    @IBAction func playAudio(_ sender: UIButton) {
        record.playSound(date: DateSingleton.shared.getDate())
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return typeExercise.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityTableViewCell", for: indexPath) as! ActivityTableViewCell
        
        cell.backgroundColor = UIColor.clear
        
        cell.heartbeatLabel.text = heartExercise[indexPath.row]
        cell.exerciseLabel.text = typeExercise[indexPath.row]
        
        return cell
    }
    
    // Done button
    @IBAction func backToCalendar(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    //Extracted Exercise
    class ExerciseRec{
        
        var date : String
        var type : String
        var heart : String
        
        public init(d: String, t: String, h: String){
            date = d
            type = t
            heart = h
        }
        
    }
    

}
