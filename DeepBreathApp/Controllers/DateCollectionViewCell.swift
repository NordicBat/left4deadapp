//
//  DateCollectionViewCell.swift
//  Left4DeadApp
//
//  Created by Di Monda Davide on 11/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var circleCurrentDay: UIImageView!
    @IBOutlet weak var exBar: UIImageView!
    
}
