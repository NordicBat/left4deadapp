//
//  RecordSound.swift
//  Left4DeadApp
//
//  Created by Gabriel de Freitas Meira on 10/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class RecordSound {
    
    var audioRecorder: AVAudioRecorder!
  
    func recordAudio(dateName: String) {
        //          creates the dir
        let dirPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask) [0] as URL
        
        //          name of the file
        let recordingName = "Audio for \(dateName).wav"
        let pathArray = dirPath.appendingPathComponent(recordingName)
        
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default)
        
        try! audioRecorder = AVAudioRecorder(url: pathArray, settings: [:])
        audioRecorder.delegate = self as? AVAudioRecorderDelegate
        audioRecorder.isMeteringEnabled = true
        audioRecorder.prepareToRecord()
        audioRecorder.record()
        
    }

    var player: AVAudioPlayer!
    func playSound(date: String) {
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) [0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComp = url.appendingPathComponent("Audio for \(date).wav"){
            let filePath = pathComp.path
            print(filePath)
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord , mode: .spokenAudio, options: .defaultToSpeaker)
                    try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
                    player = try AVAudioPlayer(contentsOf: pathComp)
                    
                    guard let player = player else { return }
                    
                    player.play()
                } catch {
                    print("Not found asdbnfajkdsf")
                }
            } else {                
                print("Didnt find the file")
            }
            
        } else {
            print("File path not Found")
        }
        
    }
    
    func stopRecording() {
        audioRecorder.stop()
        let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setActive(false)
    }
}
