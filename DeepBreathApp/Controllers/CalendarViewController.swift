//
//  StatisticsViewController.swift
//  Left4DeadApp
//
//  Created by Gabriel de Freitas Meira on 07/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CalendarViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: Outlets
    @IBOutlet weak var Calendar: UICollectionView!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var circleDayEx: UIImageView!
    @IBOutlet weak var circleCurrentDay: UIImageView!
    
    // MARK: Declarations
    let lightBlue = UIColor(red: 142/255, green: 197/255, blue: 252/255, alpha: 1)
    
    let Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    let DaysOfMonth = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    var DaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    
    var currentMonth = String()
    var numberOfEmptyBox = Int()
    var nextNumberOfEmptyBox = Int()
    var previousNumberOfEmptyBox = 0
    var direction = 0
    var positionIndex = 0
    var leapYearCount = 2
    var dayCount = 0
    var dateString = String()
    var dateSecondFormatString = String()
    
    var cellsArray : [UICollectionViewCell] = []
    var dateExercise : [String] = []
    
    
    // MARK: DidLoad
    override func viewDidLoad() {
       super.viewDidLoad()
        
        // MARK: Extracting dates from CoreData
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Exercise")
        
        request.returnsObjectsAsFaults = false
        
        do{
            
            let results = try context.fetch(request)
            
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    if let dateRec = result.value(forKey: "date") as? String{
                        // Saving the extracted data in dateExercise
                        dateExercise.append(dateRec)
                    }
                }
                print(dateExercise)
            }
            
        }catch{
            print("ERROR")
        }

        
        // Setting calendar
        Calendar.backgroundColor = UIColor.clear
        
        currentMonth = Months[month]
        monthLabel.text = "\(currentMonth) \(year)"
        
        if(weekday == 0){
            weekday = 7
        }
        
        getStartDateDayPosition()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        // Set white status bar
        return .lightContent
    }
    
    
    // MARK: next month button
    @IBAction func next(_ sender: Any) {
        
        switch currentMonth {
        case "December":
            
            direction = 1
            
            month = 0
            year += 1
            
            if leapYearCount < 5{
                leapYearCount += 1
            }
            
            if leapYearCount == 4{
                DaysInMonth[1] = 29
            }
            
            if leapYearCount == 5{
                leapYearCount = 1
                DaysInMonth[1] = 28
            }
            
            getStartDateDayPosition()
            
            currentMonth = Months[month]
            
            monthLabel.text = "\(currentMonth) \(year)"
            
            moveAnimationNext(label: monthLabel)
            
            Calendar.reloadData()
            
        default:
            
            direction = 1
            
            getStartDateDayPosition()
            
            month += 1
            
            currentMonth = Months[month]
            
            monthLabel.text = "\(currentMonth) \(year)"
            
            moveAnimationNext(label: monthLabel)
            
            Calendar.reloadData()
            
        }
        
    }
    
    
    // MARK: prev month button
    @IBAction func back(_ sender: Any) {
        
        switch currentMonth {
        case "January":
            
            direction = -1
            
            month = 11
            year -= 1
            
            if leapYearCount > 0{
                leapYearCount -= 1
            }
            if leapYearCount == 0{
                DaysInMonth[1] = 29
                leapYearCount = 4
            }else{
                DaysInMonth[1] = 28
            }
            
            getStartDateDayPosition()
            
            currentMonth = Months[month]
            
            monthLabel.text = "\(currentMonth) \(year)"
            
            moveAnimationBack(label: monthLabel)
            
            Calendar.reloadData()
            
        default:
            
            direction = -1
            
             month -= 1
            
            getStartDateDayPosition()
            
            currentMonth = Months[month]
            
            monthLabel.text = "\(currentMonth) \(year)"
            
            moveAnimationBack(label: monthLabel)
            
            Calendar.reloadData()
            
        }
        
    }
    
    
    // MARK: Calc. initial day of the month
    func getStartDateDayPosition(){
        switch direction {
        case 0:
            numberOfEmptyBox = weekday
            dayCount = day
            while dayCount > 0{
                numberOfEmptyBox = numberOfEmptyBox - 1
                dayCount = dayCount - 1
                if numberOfEmptyBox == 0{
                    numberOfEmptyBox = 7
                }
            }
            if numberOfEmptyBox == 7{
                numberOfEmptyBox = 0
            }
            positionIndex = numberOfEmptyBox
            
        case 1...:
            nextNumberOfEmptyBox = (positionIndex + DaysInMonth[month])%7
            positionIndex = nextNumberOfEmptyBox
        case -1:
            previousNumberOfEmptyBox = (7 - (DaysInMonth[month] - positionIndex)%7)
            if previousNumberOfEmptyBox == 7{
                previousNumberOfEmptyBox = 0
            }
            positionIndex = previousNumberOfEmptyBox
            
        default:
            fatalError()
        }
    }
    
    
    // MARK: Collection view settings
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch direction {
        case 0:
            return DaysInMonth[month] + numberOfEmptyBox
        case 1...:
            return DaysInMonth[month] + nextNumberOfEmptyBox
        case -1:
            return DaysInMonth[month] + previousNumberOfEmptyBox
        default:
            fatalError()
        }
        
    }
    
    
    // MARK: Dispaly cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateCollectionViewCell", for: indexPath) as! DateCollectionViewCell
        
        cell.backgroundColor = UIColor.clear
        cell.dateLabel.textColor = UIColor.white
        cell.circleCurrentDay.isHidden = true
        cell.exBar.isHidden = true
        cell.isUserInteractionEnabled = false
        
        if cell.isHidden{
            cell.isHidden = false
        }

        switch direction {
        case 0:
            cell.dateLabel.text = "\(indexPath.row + 1 - numberOfEmptyBox)"
        case 1:
            cell.dateLabel.text = "\(indexPath.row + 1 - nextNumberOfEmptyBox)"
        case -1:
            cell.dateLabel.text = "\(indexPath.row + 1 - previousNumberOfEmptyBox)"
        default:
            fatalError()
        }
        
        if Int(cell.dateLabel.text!)! < 1{
            cell.isHidden = true
        }
        
        if (dateExercise.isEmpty == false) {
            
            // Display days with exercises
            for index in 0...dateExercise.count-1{
                if("\(cell.dateLabel.text ?? "error") \(month + 1) \(year)" == dateExercise[index]){
                    cell.exBar.isHidden = false
                    cell.isUserInteractionEnabled = true
                }
            }
            
        }
        
        
        // Display current day
        if currentMonth == Months[calendar.component(.month, from: date) - 1] && year == calendar.component(.year, from: date) && indexPath.row + 1 - numberOfEmptyBox == day{
            
            cell.circleCurrentDay.isHidden = false
            cell.dateLabel.textColor = lightBlue
        }
        
        cellsArray.append(cell)
        
        return cell
    }
    
    
    // MARK: Select date of the calendar
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let cell = Calendar.cellForItem(at: indexPath) as? DateCollectionViewCell{
            
            // Test
            dateString = ("\(cell.dateLabel.text ?? "error") \(currentMonth) \(year)")
            dateSecondFormatString = "\(cell.dateLabel.text ?? "error") \(month + 1) \(year)"
//            print(dateString)
            
        }
        
        // MARK: Setting the title for the next view controller
        DateSingleton.shared.setDate(d: dateString)
//        print(DateSingleton.shared.getDate())
        DateSingleton.shared.setDateSecondFormat(d: dateSecondFormatString)
        
        Calendar.reloadData()
        
    }
    
    
    // MARK: Cells animation
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.alpha = 0
        cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
        
        for i in cellsArray{
            
            let cell : UICollectionViewCell = i
            
            UIView.animate(withDuration: 1, delay: 0.01 * Double(indexPath.row), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
                
                cell.alpha = 1
                cell.layer.transform = CATransform3DMakeScale(1, 1, 1)
                
                })
            
        }
        
    }
    
    
    // MARK: Done button
    @IBAction func backToMain(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
