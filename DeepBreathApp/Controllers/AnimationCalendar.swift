//
//  AnimationCalendar.swift
//  Left4DeadApp
//
//  Created by Di Monda Davide on 14/12/2018.
//  Copyright © 2018 gfmeira. All rights reserved.
//

import Foundation
import UIKit

// Next button animation
func moveAnimationNext(label : UILabel){
    
    let positionAnimation = CABasicAnimation(keyPath: "position")
    positionAnimation.fromValue = NSValue(cgPoint: CGPoint(x: label.center.x + 60, y: label.center.y))
    positionAnimation.toValue = NSValue(cgPoint: CGPoint(x: label.center.x, y: label.center.y))
    positionAnimation.duration = 0.3
    
    let fadeAnimation = CABasicAnimation(keyPath: "opacity")
    fadeAnimation.fromValue = 0
    fadeAnimation.toValue = 1
    fadeAnimation.duration = 0.3
    
    label.layer.add(positionAnimation, forKey: "position")
    label.layer.add(fadeAnimation, forKey: "opacity")
    
}

// Back button animation
func moveAnimationBack(label : UILabel){
    
    let positionAnimation = CABasicAnimation(keyPath: "position")
    positionAnimation.fromValue = NSValue(cgPoint: CGPoint(x: label.center.x - 60, y: label.center.y))
    positionAnimation.toValue = NSValue(cgPoint: CGPoint(x: label.center.x, y: label.center.y))
    positionAnimation.duration = 0.3
    
    let fadeAnimation = CABasicAnimation(keyPath: "opacity")
    fadeAnimation.fromValue = 0
    fadeAnimation.toValue = 1
    fadeAnimation.duration = 0.3
    
    label.layer.add(positionAnimation, forKey: "position")
    label.layer.add(fadeAnimation, forKey: "opacity")
    
}


